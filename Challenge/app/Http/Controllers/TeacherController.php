<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\TeacherTree;
use App\Http\Teacher;

class TeacherController extends Controller
{
    /**
     * Create a tree on teachers.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createTeacherTree(Request $request){
        $input = $request->input();
        $teacherTree = $this->createNodes($input);
        $teacherTree = $this->connectTree($teacherTree, $input);
        $teacher = $teacherTree->getRootTeacher(); 
        return response()->json($this->getResult($teacher));
        return json_encode($request);
    }

    /**
     * Create all the nodes in the tree.
     * @param  object  $input
     * @return TeacherTree
     */
    function createNodes($input){
        $teacherTree = new TeacherTree();
        foreach ($input as $key => $value) {
            if(!$teacherTree->getTeacherByName($key)){
                $teacherTree->createTeacher($key, null);
            }
            if(!$teacherTree->getTeacherByName($value)){
                $teacherTree->createTeacher($value, null);
            }
        }
        return $teacherTree;
    }

    /**
     * Connect teachers in the tree to each other.
     * @param  object  $input
     * @param  TeacherTree  $teacherTree
     * @return TeacherTree
     */
    function connectTree($teacherTree, $input){
        foreach ($input as $key => $value) {
            $supervisor = $teacherTree->getTeacherByName($value);
            $teacher = $teacherTree->getTeacherByName($key);
            $teacher->setSupervisor($supervisor);
            $supervisor->addSupervising($teacher);
        }
        return $teacherTree;
    }

    /**
     * Get the nested object of teacher.
     * @param  Teacher  $teacher
     * @return Array
     */
    public function getResult($teacher){
        $array = [];
        foreach ($teacher->getSupervising() as $value) {
            array_push($array, $this->getResult($value));
        }
        $name = $teacher->getName();
        return array($name => $array);
    }

    
}
