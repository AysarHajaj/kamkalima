<?php

namespace App\Http;


class Teacher
{
    private $name = '';
    private $supervising = [];
    private $supervisor = null;

    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * Add teacher to the list of teacher who supervised by this teacher.
     *
     * @param  \Illuminate\Http\Teacher  $teacher
     * @return 
     */
    public function addSupervising($teacher){
        array_push($this->supervising, $teacher);
    }

    /**
     * Set name for this teacher.
     *
     * @param  String  $name
     * @return 
     */
    public function setName($name){
        $this->name = $name;
    }

    /**
     * Set supervisor for this teacher.
     *
     * @param  \Illuminate\Http\Teacher  $supervisor
     * @return 
     */
    public function setSupervisor($supervisor){
        $this->supervisor = $supervisor;
    }

    /**
     * Set list of teacher who supervised by this teacher.
     *
     * @param  List  $supervisiong
     * @return 
     */
    public function setSupervising($supervising){
        $this->supervising = $supervising;
    }

    /**
     * Return the name of this teacher.
     * 
     * @return Stirng
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Return the supervisor of this teacher.
     * 
     * @return \Illuminate\Http\Teacher
     */
    public function getSupervisor(){
        return $this->supervisor;
    }

    /**
     * Return list of teacher who supervised by this teacher.
     * 
     * @return List
     */
    public function getSupervising(){
        return $this->supervising;
    }
}