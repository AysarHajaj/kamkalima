<?php

namespace App\Http;


class TeacherTree
{
    private $teachers = [];

    /**
     * Return the supervisor of this teacher.
     * 
     * @param  String  $name
     * @return \Illuminate\Http\Teacher
     */
    function getTeacherByName($name){
        foreach ($this->teachers as $teacher) {
            if($teacher->getName() == $name){
                return $teacher;
            }
        }
        return null;
    }

    /**
     * Return teacher who have no supervisor.
     * 
     * @return \Illuminate\Http\Teacher
     */
    function getRootTeacher(){
        foreach ($this->teachers as $teacher) {
            if(!$teacher->getSupervisor()){
                return $teacher;
            }
        }
        return null;
    }

    /**
     * Return the supervisor of this teacher.
     * 
     * @param  String  $name
     * @param  \Illuminate\Http\Teacher  $supervisor
     * @return
     */
    function createTeacher($name, $supervisor){
        $teacher = new Teacher($name);
        if($supervisor){
            $teacher->setSupervisor(node);
        }
        array_push($this->teachers, $teacher);
    }

    /**
     * Set list of teacher.
     *
     * @param  List  $supervisiong
     * @return 
     */
    public function setTeachers($teachers){
        $this->teachers = $teachers;
    }

    /**
     * Return list of teachers.
     * 
     * @return List
     */
    public function getTeachers(){
        return $this->teachers;
    }
}
