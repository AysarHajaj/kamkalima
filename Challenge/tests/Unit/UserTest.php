<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * Test request status.
     *
     * @return void
     */
    public function testStatus()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Test Json Post request with the example data.
     *
     * @return void
     */
    public function testJson()
    {
        $response = $this->json('POST', '/api/result', ['Rana' => 'Hiba',
                                                    'Samar' => 'Sara',
                                                    'Hassan' => 'Sara',
                                                    'Sara' => 'Hiba',
                                                    'Hiba' => 'Siroun']);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'Siroun' => [
                    ['Hiba' => [
                        ['Rana' => []],
                        ['Sara' => [
                            ['Samar' => []],
                            ['Hassan' => []]
                        ]]
                    ]]
                ],
            ]);
    }
}
