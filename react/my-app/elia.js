import React, { Component } from 'react';

export default class elia extends Component {
  render() {
    return (
      <div className="elia">
        { this.props.children }
      </div>
    )
  }
}
