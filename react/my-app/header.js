import React, { Component } from 'react';

export default class header extends Component {
  render() {
    return (
      <div className="header">
        { this.props.children }
      </div>
    )
  }
}
